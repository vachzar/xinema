@extends('layout')

@section('content')
    <h2 class="cover-heading  text-center">{{ $movie['Title'] }} ({{ $movie['Year'] }})</h2>
        <div class="row justify-content-md-center">
          <form class="form-inline" method="post" action="{{ url('player/' . Str::slug($movie['Title'] . $movie['Year'], '-') . '/' . $movie['imdbID']) }}">
              @csrf
            <div class="form-group mb-2">
              <label for="substitle" class="sr-only">Add Subtitle</label>
              <input type="text" name="substitle" class="form-control" id="substitle">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Add Subtitle</button>          
          </form>
      </div>
      <div class="row justify-content-md-center">                
        <small id="forSubstitle" class="form-text text-muted">
         *Support subscene.com link. ex: https://subscene.com/subtitles/bad-boys-for-life/english/2172695
        </small>
      </div>
      <div id="loadedSubInfo" class="row justify-content-md-center"></div>
        <iframe id="jarframe" src="https://gifimage.net/wp-content/uploads/2018/11/loading-gif-cdn-2.gif" data-movie-id="{{ $movie['imdbID'] }}" data-movie-sub="{{ $substitle ?? '' }}" frameborder="0" width="100%" height="640" allowfullscreen="allowfullscreen"></iframe>
@endsection

@push('scripts')
    <script src="/js/player.js"></script>
@endpush