@extends('layout')

@section('content')
    <h2 class="cover-heading text-center">Search @if ($keyword) of: {{ $keyword }} @endif</h2>
    <div class="row justify-content-sm-center">
        <form class="form-inline" method="post" action="{{ url('search') }}">
            @csrf
          <div class="form-group mb-2">
            <label for="keyword" class="sr-only">Title</label>
            <input type="text" name="keyword" class="form-control" id="keyword">
          </div>
          <button type="submit" class="btn btn-primary mb-2">Search</button>
        </form>
    </div>
    @isset ($movies)
        @foreach ($movies as $movie)   
          
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="media">
              <img src="{{ Muvi::getPosterUrl($movie['poster']) }}" class="mr-3 img-fluid" style="max-width: 150px">
              <div class="media-body">
                <h5 class="mt-0"><a href="{{ url('player/' . Str::slug($movie['title'] . $movie['year'], '-') . '/' . $movie['imdb']) }}">{{ $movie['title'] }} ({{ $movie['year'] }})</a></h5>
                Subtitle: {{ $movie['sub'] }} <br>
                Quality : {{ $movie['quality'] }} <br>
                Genre : {{ $movie['genre'] }} <br>
                Runtime : {{ $movie['runtime'] }} <br>
                IMDb Rating: {{ $movie['rating'] }}
              </div>
            </div>
            <hr>
        </div>
    </div>
        @endforeach 
    @endisset
@endsection