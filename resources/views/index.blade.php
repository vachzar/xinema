@extends('layout')

@section('content')
    <h2 class="cover-heading text-center">Latest Added Movie</h2>
        @foreach ($data as $movie)   
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="media">
                  <img src="{{ Muvi::getPosterUrl($movie['poster']) }}" class="mr-3 img-fluid" style="max-width: 150px">
                  <div class="media-body">
                    <h5 class="mt-0"><a href="{{ url('player/' . Str::slug($movie['title'] . $movie['year'], '-') . '/' . $movie['imdb']) }}">{{ $movie['title'] }} ({{ $movie['year'] }})</a></h5>
                    Subtitle: {{ $movie['sub'] }} <br>
                    Quality : {{ $movie['quality'] }} <br>
                    Genre : {{ $movie['genre'] }} <br>
                    Runtime : {{ $movie['runtime'] }} <br>
                    IMDb Rating: {{ $movie['rating'] }}
                  </div>
                </div>
                <hr>
            </div>
        </div>
        @endforeach 
@endsection