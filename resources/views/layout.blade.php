<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:url"                content="{{ url('/') }}" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="VXinema" />
        <meta property="og:description"        content="Streaming Film by SobatRebahanProduction" />
        <meta property="og:image"              content="{{ url('mitsushima.jpg') }}" />
        <title>VXinema</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
         @stack('style')        
    </head>
    <body>
        <div class="container d-flex w-100 h-100 p-3 mx-auto flex-column"> 
            <header class="masthead mb-auto">
                <div class="inner text-center">
                    <img src="{{ url('mitsushima.jpg') }}" class="img-fluid">
                    <h1 class="masthead-brand">VXinema 0.1</h1>
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-center">
                        <div class="navbar-nav">
                            <a class="nav-item nav-link" href="{{ url('/') }}">Home</a>
                            <a class="nav-item nav-link" href="{{ url('search') }}">Search</a>
                        </div>
                    </nav>
                </div>
              </header>

            <main role="main" class="inner cover">
                 @yield('content')
            </main>
            <footer class="mastfoot mt-auto">
                <div class="inner text-center">
                  <p>VXinema 0.1 &copy; by <a href="https://vachzar.com/">vachzar</a>.</p>
                </div>
            </footer>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
        @stack('scripts')
    </body>
</html>
