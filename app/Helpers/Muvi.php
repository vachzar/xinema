<?php
namespace App\Helpers;



class Muvi {
    public static function getPosterUrl($path) {
    	if (strpos($path, 'gdriveplayer') === false) {
    		return $path;
    	} else {    		
	        $duar = explode('/', $path);
			$filename = end($duar);
	        return url('cover/' . $filename);
    	}
    }
}