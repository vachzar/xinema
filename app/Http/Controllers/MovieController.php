<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class MovieController extends Controller
{
    //
    public function index()
	{
	    $response = Http::get('https://api.gdriveplayer.us/v1/movie/newest');
	    $data = $response->json();
	    return view('index',compact('data'));
	}

	public function player(Request $request, $title, $imdbID, $sub = '')
	{
		if ($request->isMethod('post')) {
			$data['substitle'] = $request->input('substitle');
		}
		$response = Http::get('https://api.gdriveplayer.us/v1/imdb/' . $imdbID);
	    $data['movie'] = $response->json();
		$data['imdbID'] = $imdbID;
	    return view('player', $data);
	}

	public function search(Request $request)
	{	
		$data['keyword'] = '';
		if ($request->isMethod('post')) {
			$data['keyword'] = $request->input('keyword');
			$response = Http::get('https://api.gdriveplayer.us/v1/movie/search?title=' . $data['keyword']);
		    $data['movies'] = $response->json();
		}
	    return view('search', $data);
	}

	public function cover($filename){
		$response = Http::get('https://database.gdriveplayer.us/cover/' . $filename);
	    $cover = $response->body();
	    $duar = explode('.', $filename);
		$ext = end($duar);
	    return response($cover)->header('Content-Type', 'image/' . $ext)
	    	   ->header('Pragma','public')
	           ->header('Cache-Control','max-age=60, must-revalidate');

	}
}
